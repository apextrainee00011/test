from .views import *
from django.urls import path, include
from rest_framework_simplejwt import views as jwt_views
from .serializers import *
  
urlpatterns = [
    path('token/',jwt_views.TokenObtainPairView.as_view(serializer_class=CustomJWTSerializer),name ='token_obtain_pair'),
    path('token/refresh/',jwt_views.TokenRefreshView.as_view(),name ='token_refresh'),
    path("verifytoken/",jwt_views.TokenVerifyView.as_view(),name="veryfytoken"),
    path("",register,name="register"),
    path("login",login,name="login"),
    path("home",homepage,name="homepage"),
    path("logout",logout,name="logout"),
    path("<int:pk>/",userdetail.as_view(),name="detail"),
    path("<int:pk>/update",userupdate.as_view(),name="update"),
    path("<int:pk>/delete",userdelete.as_view(),name="delete"),
]