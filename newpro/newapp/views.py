from django.contrib import auth
from django.contrib.auth.hashers import make_password
from django.shortcuts import render,redirect,reverse
from django.views import generic
from .models import *
from .forms import *
from rest_framework_simplejwt import views as jwt_views
from rest_framework_simplejwt.tokens import RefreshToken

def homepage(request):
    userdetail=User.objects.all()
    
    return render(request,'home.html',{'userdetail':userdetail})        

class userdetail(generic.DetailView):
    template_name='detail_view.html'
    context_object_name="detail"
    def get_queryset(self):
        query_set=User.objects.all() 
        return query_set

class userdelete(generic.DeleteView):
    template_name='delete_view.html'
    
    query_set=User.objects.all()
    def get_queryset(self):
        query_set=User.objects.all()
        return query_set
    def get_success_url(self):
        return reverse("homepage")

class userupdate(generic.UpdateView):
    template_name='update_view.html'
    form_class=usermodelform
    query_set=User.objects.all()
    def get_queryset(self):
        query_set=User.objects.all()
        return query_set
    def get_success_url(self):
        return reverse("homepage")

def register(request):
    form=signupform()
    if request.method=='POST':
        form=signupform(request.POST)
        if form.is_valid():
            username=request.POST['username']
            email=request.POST['email']
            password=request.POST['password']
            confirm_password=request.POST['confirm_password']
            address=request.POST['address']
            if password==confirm_password:
                if User.objects.filter(username=username).exists():
                    form=signupform()            
                    return render(request,'register.html',{'form':form})
                elif User.objects.filter(email=email).exists():
                    form=signupform()            
                    return render(request,'register.html',{'form':form})
                else:
                    password=make_password(password)
                    user=User.objects.create(username=username,email=email,password=password,address=address)
                    user.save();
                    return redirect("/login")
            else:
                form=signupform()            
            return render(request,'register.html',{'form':form})
        else:
            form=signupform()            
        return render(request,'register.html',{'form':form})                    
    else:
        form=signupform()            
        return render(request,'register.html',{'form':form})

def login(request):
    if request.method=='POST':
        email=request.POST['username']
        password=request.POST['password']
        if User.objects.filter(email=email).exists():
            usr=User.objects.get(email=email)
            username=usr.username
            user=auth.authenticate(username=username,password=password)
            print(username)
            print(password)

            if user is not None:
                # res=jwt_views.TokenObtainPairView(username=username,password=password)
                # print(res)
                refresh = RefreshToken.for_user(user)

                token={'refresh': str(refresh),'access': str(refresh.access_token)}
                return render(request,'res.html',{'token':token})
                
                
                #return render(request,'res.html',{'res':res})
            else:
                   
                return render(request,"login.html")
        else:
                
            return render(request,"login.html")
    else:
        return render(request,"login.html")


def logout(request):
    auth.logout(request)
    return redirect('/home')