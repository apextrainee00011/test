from django.db import models
from django.contrib.auth.models import AbstractUser

class User(AbstractUser):
    email=models.CharField(unique=True,max_length=200)
    address=models.CharField(max_length=200)