from django import forms
from .models import *
class usermodelform(forms.ModelForm):
    class Meta:
        model=User
        fields={'id','username','email','address'}

class signupform(forms.Form):
    username=forms.CharField()
    email=forms.EmailField()
    password=forms.CharField(widget=forms.PasswordInput())
    confirm_password=forms.CharField(widget=forms.PasswordInput())
    address=forms.CharField()

# class loginform(forms.Form):
#     username=forms.EmailField()
#     password=forms.CharField(widget=forms.PasswordInput())